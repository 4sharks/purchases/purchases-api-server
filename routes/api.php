<?php

use App\Http\Controllers\Inv\InvController;
use App\Http\Controllers\Inv\InvReturnsController;
use App\Http\Controllers\Inv\PaymentMethodController;
use App\Http\Controllers\Inv\PaymentTransactionController;
use App\Http\Controllers\Quote\QuoteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Quotes Routes
Route::get('/purchases/quotes/{tenant_id}/{company_id}/{branch_id}',[QuoteController::class, 'index']);
Route::post('/purchases/quotes',[QuoteController::class, 'store']);
Route::get('/purchases/quotes/show/{id}',[QuoteController::class, 'show']);
Route::get('/purchases/quotes/{tenant_id}/{company_id}/{branch_id}/{inv_no}',[QuoteController::class, 'getByInvNo']);
Route::put('/purchases/quotes/update/{id}',[QuoteController::class, 'update']);
Route::delete('/purchases/quotes/delete/{id}',[QuoteController::class, 'destroy']);
Route::post('/purchases/quotes/get-last-inv-no-by-tenant',[QuoteController::class, 'get_last_invno_by_tenant']);
Route::post('/purchases/quotes/get-last-inv-no-by-tenant-with-prefix',[QuoteController::class, 'get_last_invno_by_tenant_with_prefix']);
Route::get('/purchases/quotes/customers/{tenant_id}/{company_id}/{branch_id}/{customer_id}',[QuoteController::class, 'getByCustomer']);

Route::get('/purchases/quotes/month/{tenant_id}/{company_id}/{branch_id}/{month}',[QuoteController::class, 'sumInvoicesByMonth']);
Route::get('/purchases/quotes/year/{tenant_id}/{company_id}/{branch_id}/{year}',[QuoteController::class, 'sumInvoicesByYear']);
Route::get('/purchases/quotes/paid/{tenant_id}/{company_id}/{branch_id}/{year}/{is_paid}',[QuoteController::class, 'sumInvoicesPaidByYear']);
Route::get('/purchases/quotes/month-group/{tenant_id}/{company_id}/{branch_id}/{year}',[QuoteController::class, 'sumGroupByMonth']);
Route::get('/purchases/quotes/salesman/{tenant_id}/{company_id}/{branch_id}/{year}',[QuoteController::class, 'sumBySalesman']);


// Invoice Routes
Route::get('/purchases/invoices/{tenant_id}/{company_id}/{branch_id}',[InvController::class, 'index']);
Route::post('/purchases/invoices',[InvController::class, 'store']);
Route::get('/purchases/invoices/show/{id}',[InvController::class, 'show']);
Route::get('/purchases/invoices/{tenant_id}/{company_id}/{branch_id}/{inv_no}',[InvController::class, 'getByInvNo']);
Route::put('/purchases/invoices/update/{id}',[InvController::class, 'update']);
Route::delete('/purchases/invoices/delete/{id}',[InvController::class, 'destroy']);
Route::post('/purchases/invoices/get-last-inv-no-by-tenant',[InvController::class, 'get_last_invno_by_tenant']);
Route::post('/purchases/invoices/get-last-inv-no-by-tenant-with-prefix',[InvController::class, 'get_last_invno_by_tenant_with_prefix']);
Route::get('/purchases/invoices/customers/{tenant_id}/{company_id}/{branch_id}/{customer_id}',[InvController::class, 'getByCustomer']);
Route::post('/purchases/invoices/set-paid/{id}',[InvController::class, 'setPaid']);

Route::get('/purchases/invoices/month/{tenant_id}/{company_id}/{branch_id}/{month}',[InvController::class, 'sumInvoicesByMonth']);
Route::get('/purchases/invoices/year/{tenant_id}/{company_id}/{branch_id}/{year}',[InvController::class, 'sumInvoicesByYear']);
Route::get('/purchases/invoices/paid/{tenant_id}/{company_id}/{branch_id}/{year}/{is_paid}',[InvController::class, 'sumInvoicesPaidByYear']);
Route::get('/purchases/invoices/month-group/{tenant_id}/{company_id}/{branch_id}/{year}',[InvController::class, 'sumGroupByMonth']);
Route::get('/purchases/invoices/salesman/{tenant_id}/{company_id}/{branch_id}/{year}',[InvController::class, 'sumBySalesman']);
Route::get('/purchases/invoices/topProducts/{tenant_id}/{company_id}/{branch_id}/{year}',[InvController::class, 'topProducts']);
Route::post('/purchases/invoices/filterBy',[InvController::class, 'getFilterBy']);

// Invoice Returns Routes
Route::get('/purchases/invoices-returns/{tenant_id}/{company_id}/{branch_id}',[InvReturnsController::class, 'index']);
Route::post('/purchases/invoices-returns',[InvReturnsController::class, 'store']);
Route::get('/purchases/invoices-returns/show/{id}',[InvReturnsController::class, 'show']);
Route::get('/purchases/invoices-returns/{tenant_id}/{company_id}/{branch_id}/{inv_no}',[InvReturnsController::class, 'getByInvNo']);
Route::put('/purchases/invoices/update/{id}',[InvReturnsController::class, 'update']);
Route::delete('/purchases/invoices-returns/delete/{id}',[InvReturnsController::class, 'destroy']);
Route::post('/purchases/invoices-returns/get-last-inv-no-by-tenant',[InvReturnsController::class, 'get_last_invno_by_tenant']);
Route::post('/purchases/invoices-returns/get-last-inv-no-by-tenant-with-prefix',[InvReturnsController::class, 'get_last_invno_by_tenant_with_prefix']);
Route::get('/purchases/invoices-returns/customers/{tenant_id}/{company_id}/{branch_id}/{customer_id}',[InvReturnsController::class, 'getByCustomer']);
Route::post('/purchases/invoices-returns/set-paid/{id}',[InvReturnsController::class, 'setPaid']);

Route::get('/purchases/invoices-returns/month/{tenant_id}/{company_id}/{branch_id}/{month}',[InvReturnsController::class, 'sumInvoicesByMonth']);
Route::get('/purchases/invoices-returns/year/{tenant_id}/{company_id}/{branch_id}/{year}',[InvReturnsController::class, 'sumInvoicesByYear']);
Route::get('/purchases/invoices-returns/paid/{tenant_id}/{company_id}/{branch_id}/{year}/{is_paid}',[InvReturnsController::class, 'sumInvoicesPaidByYear']);
Route::get('/purchases/invoices-returns/month-group/{tenant_id}/{company_id}/{branch_id}/{year}',[InvReturnsController::class, 'sumGroupByMonth']);
Route::get('/purchases/invoices-returns/salesman/{tenant_id}/{company_id}/{branch_id}/{year}',[InvReturnsController::class, 'sumBySalesman']);
Route::get('/purchases/invoices-returns/topProducts/{tenant_id}/{company_id}/{branch_id}/{year}',[InvReturnsController::class, 'topProducts']);
Route::post('/purchases/invoices-returns/filterBy',[InvReturnsController::class, 'getFilterBy']);

// Payment Methods Routes
Route::get('/purchases/payment-methods/{tenant_id}/{company_id}/{branch_id}',[PaymentMethodController::class,'index']);
Route::post('/purchases/payment-methods',[PaymentMethodController::class,'store']);
Route::put('/purchases/payment-methods/update/{id}',[PaymentMethodController::class,'update']);
Route::delete('/purchases/payment-methods/delete/{id}',[PaymentMethodController::class,'destroy']);

// Payment Transactions Routes
Route::get('/purchases/payment-transactions/{tenant_id}/{company_id}/{branch_id}',[PaymentTransactionController::class,'index']);
Route::get('/purchases/payment-transactions/invoice/{inv_id}',[PaymentTransactionController::class,'getTrannsactionsByInvId']);
Route::post('/purchases/payment-transactions',[PaymentTransactionController::class,'store']);
Route::post('/purchases/payment-transactions/filter',[PaymentTransactionController::class,'filterBy']);
