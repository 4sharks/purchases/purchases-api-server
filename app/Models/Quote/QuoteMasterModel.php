<?php

namespace App\Models\Quote;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuoteMasterModel extends Model
{
    use HasFactory;


    protected $table = 'quotes_master';
    protected $guarded = [];

    public function quoteDetails(){
        return $this->hasMany(QuoteDetailModel::class,'inv_id' ,'id');
    }


}
