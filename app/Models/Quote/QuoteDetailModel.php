<?php

namespace App\Models\Quote;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuoteDetailModel extends Model
{
    use HasFactory;

    protected $table = 'quotes_details';
    protected $guarded = [];

}
