<?php

namespace App\Models\Inv;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvMasterModel extends Model
{
    use HasFactory;

    protected $table = 'inv_master';
    protected $guarded = [];

    public function invDetails(){
        return $this->hasMany(InvDetailModel::class,'inv_id' ,'id');
    }

}
