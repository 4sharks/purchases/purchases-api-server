<?php

namespace App\Models\Inv;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvDetailModel extends Model
{
    use HasFactory;


    protected $table = 'inv_details';

    protected $guarded = [];

}
