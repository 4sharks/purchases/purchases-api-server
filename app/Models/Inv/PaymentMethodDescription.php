<?php

namespace App\Models\Inv;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethodDescription extends Model
{
    use HasFactory;
    protected $table = 'payment_method_description';
    protected $guarded = [];
}
