<?php

namespace App\Models\Inv;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvReturnsDetailModel extends Model
{
    use HasFactory;

    protected $table = 'inv_returns_details';

    protected $guarded = [];
}
