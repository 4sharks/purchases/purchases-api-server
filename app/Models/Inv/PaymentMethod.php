<?php

namespace App\Models\Inv;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use HasFactory;
    protected $table = 'payment_method';
    protected $guarded = [];

    public function description(){
        return $this->hasMany(PaymentMethodDescription::class,'payment_method_id','id');
    }
}
