<?php

namespace App\Models\Inv;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    use HasFactory;

    protected $table = 'payment_transactions';

    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    // protected function createdAt(): Attribute
    // {
    //     return Attribute::make(
    //         get: fn(string $value) => date('Y-m-d H:i:s',$value),
    //     );
    // }

    public function payment_method(){
        return $this->hasOne(PaymentMethod::class,'id','payment_method_id');
    }



}
