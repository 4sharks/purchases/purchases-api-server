<?php
namespace App\Services;

use App\Models\Inv\InvDetailModel;
use App\Models\Inv\InvMasterModel;
use App\Models\Inv\InvReturnsDetailModel;
use App\Models\Inv\InvReturnsMasterModel;
use Illuminate\Support\Facades\DB;

class InvReturnsService {

    public function getAllInvoices($tenant_id,$company_id,$branch_id)
    {
        return InvReturnsMasterModel::with('invDetails')->where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id
        ])->orderBy('id','desc')->paginate(21);
    }

    public function showInv($id)
    {
        return InvReturnsMasterModel::with('invDetails')->where([
            'inv_id' => $id
        ])->first();
    }

    public function createInv($request)
    {
        $inv_master = new InvReturnsMasterModel();
        $inv_master->inv_id = $request->input('inv_id');
        $inv_master->inv_no = $request->input('inv_no');
        $inv_master->inv_ref = $request->input('inv_ref');
        $inv_master->inv_date = $request->input('inv_date');
        $inv_master->inv_date_due = $request->input('inv_date_due');
        $inv_master->total_amount = $request->input('total_amount');
        $inv_master->discount_amount = $request->input('discount_amount');
        $inv_master->promo_code_id = $request->input('promo_code_id');
        $inv_master->promo_code = $request->input('promo_code');
        $inv_master->vat_amount = $request->input('vat_amount');
        $inv_master->net_amount = $request->input('net_amount');
        $inv_master->type_id = $request->input('type_id');
        $inv_master->status_id = $request->input('status_id');
        $inv_master->is_paid = $request->input('is_paid');
        $inv_master->notes = $request->input('notes');
        $inv_master->inv_tags = $request->input('inv_tags');
        $inv_master->customer_id = $request->input('customer_id');
        $inv_master->customer_name = $request->input('customer_name');
        $inv_master->salesman_id = $request->input('salesman_id');
        $inv_master->salesman_name = $request->input('salesman_name');
        $inv_master->tenant_id = $request->input('tenant_id');
        $inv_master->company_id = $request->input('company_id');
        $inv_master->branch_id = $request->input('branch_id');
        $inv_master->created_by = $request->input('created_by');

        $inv_master_store = $inv_master->save();

        if($inv_master_store){
            if($request->inv_details){
                $detail_list = array();
                $details_ids = array();
                foreach($request->inv_details as $item){

                    $inv_detail = new InvReturnsDetailModel();
                    $inv_detail->inv_returns_master_id =  $inv_master->id;
                    $inv_detail->inv_id =  $inv_master->inv_id;
                    $inv_detail->inv_no =  $inv_master->inv_no;
                    $inv_detail->product_id =  $item['product_id'];
                    $inv_detail->product_name =  $item['product_name'];
                    $inv_detail->unit_id =  $item['unit_id'];
                    $inv_detail->qty =  $item['qty'];
                    $inv_detail->price =  $item['price'];
                    $inv_detail->total_price =  $item['total_price'];
                    $inv_detail->product_discount =  $item['product_discount'];
                    $inv_detail->product_net_total =  $item['product_net_total'];
                    $inv_detail->product_vat =  $item['product_vat'];
                    $inv_detail->product_net_total_with_vat =  $item['product_net_total_with_vat'];
                    $inv_detail->save();
                    $detail_list[] = $inv_detail;
                    $details_ids[] = $item['id'];
                }
                $inv_master['inv_details'] = $detail_list;
                InvMasterModel::find($inv_master->inv_id)->update([
                    'is_returned' => true,
                    'count_returned' => count($detail_list)
                ]);
                InvDetailModel::whereIn('id',$details_ids)->update([
                    'is_returned' => true,
                ]);
            }
            return $inv_master;
        }

    }

    public function updateInv($request , $id)
    {
        $inv_master = InvReturnsMasterModel::find($id);
        if($inv_master){
        $inv_master->inv_no = $request->input('inv_no');
        $inv_master->inv_ref = $request->input('inv_ref');
        $inv_master->inv_date = $request->input('inv_date');
        $inv_master->inv_date_due = $request->input('inv_date_due');
        $inv_master->total_amount = $request->input('total_amount');
        $inv_master->discount_amount = $request->input('discount_amount');
        $inv_master->promo_code_id = $request->input('promo_code_id');
        $inv_master->promo_code = $request->input('promo_code');
        $inv_master->vat_amount = $request->input('vat_amount');
        $inv_master->net_amount = $request->input('net_amount');
        $inv_master->type_id = $request->input('type_id');
        $inv_master->status_id = $request->input('status_id');
        $inv_master->is_paid = $request->input('is_paid');
        $inv_master->notes = $request->input('notes');
        $inv_master->inv_tags = $request->input('inv_tags');
        $inv_master->customer_id = $request->input('customer_id');
        $inv_master->customer_name = $request->input('customer_name');
        $inv_master->salesman_id = $request->input('salesman_id');
        $inv_master->salesman_name = $request->input('salesman_name');
        $inv_master->tenant_id = $request->input('tenant_id');
        $inv_master->company_id = $request->input('company_id');
        $inv_master->branch_id = $request->input('branch_id');
        $inv_master->created_by = $request->input('created_by');

        $inv_master_store = $inv_master->save();

        if($inv_master_store){
            if($request->inv_details){
                $detail_list = array();
                InvReturnsDetailModel::where([
                    'inv_id' => $id
                ])->delete();
                foreach($request->inv_details as $item){
                   // $inv_detail = InvDetailModel::find($item['id']);
                    $inv_detail = new InvReturnsDetailModel();
                    $inv_detail->inv_id =  $inv_master->id;
                    $inv_detail->inv_no =  $inv_master->inv_no;
                    $inv_detail->product_id =  $item['product_id'];
                    $inv_detail->product_name =  $item['product_name'];
                    $inv_detail->unit_id =  $item['unit_id'];
                    $inv_detail->qty =  $item['qty'];
                    $inv_detail->price =  $item['price'];
                    $inv_detail->total_price =  $item['total_price'];
                    $inv_detail->product_discount =  $item['product_discount'];
                    $inv_detail->product_net_total =  $item['product_net_total'];
                    $inv_detail->product_vat =  $item['product_vat'];
                    $inv_detail->product_net_total_with_vat =  $item['product_net_total_with_vat'];
                    $inv_detail->save();
                    $detail_list[] = $inv_detail;

                }
                $inv_master['inv_details'] = $detail_list;
            }
            return $inv_master;
            }
        }
    }

    public function deleteInv($id){
        $inv_master = InvReturnsMasterModel::find($id);
        if($inv_master){
            InvReturnsDetailModel::where([
                "inv_id" => $id
            ])->delete();
            return  $inv_master->delete();
        }
        return false;
    }

    public function deleteInvDetail($id)
    {
        return InvReturnsDetailModel::find($id)->delete();
    }

    public function setPaid($request,$id){
        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');

        $inv = InvReturnsMasterModel::where([
            'id' => $id,
            'tenant_id' => $tenant_id,
            'company_id' => $company_id,
            'branch_id' => $branch_id,
        ])->update([
            'is_paid' => true
        ]);
        if($inv ){
            return $inv;
        }
        return false;
    }

    public function getLastInvNoByTenant($tenant_id,$company_id,$branch_id,$type='without_prefix',$prefix=''){
        $where = [
            'tenant_id' => $tenant_id,
            'company_id' => $company_id,
            'branch_id' => $branch_id
        ];

        if($type == 'with_prefix'){
            $fetch = InvReturnsMasterModel::where($where)->where('inv_no','LIKE',$prefix.'%')->orderBy('id','desc')->get()->first();
            if($fetch){
                return $fetch->inv_no;
            }else{
                return $prefix.'0';
            }
        }else{
            $fetch = InvReturnsMasterModel::where($where)->orderBy('id','desc')->get()->first();
            if($fetch){
                return $fetch->inv_no;
            }else{
                return 0;
            }
        }
    }

    public function getSumInvoicesByMonth($tenant_id,$company_id,$branch_id,$month)
    {
        return InvReturnsMasterModel::where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id,
        ])->whereMonth('inv_date',$month)->sum('total_amount');
    }
    public function getSumInvoicesByYear($tenant_id,$company_id,$branch_id,$year)
    {
        return InvReturnsMasterModel::where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id,
        ])->whereYear('inv_date',$year)->sum('total_amount');
    }

    public function getSumInvoicesPaidByYear($tenant_id,$company_id,$branch_id,$year,$is_paid)
    {
        return InvReturnsMasterModel::where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id,
            'is_paid' => $is_paid,
        ])->whereYear('inv_date',$year)->sum('total_amount');
    }

    public function getSumGroupByMonth($tenant_id,$company_id,$branch_id,$year)
    {
        $data =  InvReturnsMasterModel::selectRaw(' DATE_TRUNC(\'month\', inv_date) as month, SUM(total_amount) as total')
        ->where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id
        ])
        ->whereYear('inv_date',$year)
        ->groupBy('month')
        ->get();
        $new = [];
        foreach ($data as $item){
            $formattedDate = date('m', strtotime($item->month));
            $new[] = [
                'month' => $formattedDate,
                'total' => $item->total
            ];
        }
        $months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        $month_arr = [];
        $i=0;
        foreach ($months as $m){
            foreach ($new as $n){
                if($n['month'] == $m){
                    $month_arr[$i] = $n['total'];

                    break;
                }else{
                    $month_arr[$i] = 0;
                }
            }
            $i++;
        }
        return $month_arr;
    }

    public function getSumBySalesman($tenant_id,$company_id,$branch_id,$year)
    {
        $data = InvReturnsMasterModel::selectRaw('salesman_name as salesman, SUM(total_amount) as total')
        ->where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id
        ])->whereYear('inv_date',$year)->groupBy('salesman')->get();
        $amount = [];
        $labels = [];
        $i = 0;
        foreach($data as $d){
            $amount[$i] = $d['total'];
            $labels[$i] = $d['salesman'] ? $d['salesman'] : 'No Salesman';
            $i++;
        }
        return [
            'total' => $amount,
            'salesman' => $labels
        ];
    }

    public function getTopProducts($tenant_id,$company_id,$branch_id,$year)
    {
        $data = InvReturnsMasterModel::select('inv_master.*', DB::raw('inv_details.product_name, SUM(inv_details.total_price) as total'))
        ->where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id
        ])
        ->whereYear('inv_date',$year)
        ->join('inv_details',function($join){
            $join->on('inv_details.inv_id','=','inv_master.id');
        })->groupBy(['inv_details.product_name','inv_master.id'])
        ->get()->toArray();

        $result = array_reduce($data,function($carry,$item){
            if(!isset($carry[$item['product_name']])){
                $carry[$item['product_name']] = ['product_name' => $item['product_name'] ,'total' => $item['total']];
            }else{
                $carry[$item['product_name']]['total'] += $item['total'];
            }
            return $carry;
        });

        $amount = [];
        $labels = [];
        $i = 0;
        foreach($result as $d){
            if($d['total'] > 0 && $i < 10){
                $amount[$i] = $d['total'];
                $labels[$i] = $d['product_name'] ? $d['product_name'] : 'No product name';
                $i++;
            }
        }
        return [
            'total' => $amount,
            'productName' => $labels
        ];



    }

    public function getByInvNo($tenant_id,$company_id,$branch_id,$inv_no)
    {
        return InvReturnsMasterModel::where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id,
        ])->where('inv_no','like','%'.$inv_no.'%')->orderBy('id','desc')->limit(20)->get();
    }

    public function getByCustomer($tenant_id,$company_id,$branch_id,$customer_id)
    {
        return InvReturnsMasterModel::where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id,
            'customer_id' => $customer_id
        ])->orderBy('id','desc')->paginate(25);
    }

    public function filterBy($tenant_id,$company_id,$branch_id,$customer_id,$salesman_id,$inv_start_date,$inv_end_date)
    {
        if(!$tenant_id && !$company_id && !$branch_id ){
            return false;
        }

        $cond = [
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id,
        ];

        if($salesman_id){
            $cond['salesman_id'] = $salesman_id;
        }
        if($customer_id){
            $cond['customer_id'] = $customer_id;

        }
        return InvReturnsMasterModel::where($cond)->whereBetween('inv_date',[$inv_start_date,$inv_end_date])->orderBy('id','desc')->paginate(25);
    }

}
