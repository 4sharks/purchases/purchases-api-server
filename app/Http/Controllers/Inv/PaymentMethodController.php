<?php

namespace App\Http\Controllers\Inv;

use App\Http\Controllers\Controller;
use App\Models\Inv\PaymentMethod;
use App\Models\Inv\PaymentMethodDescription;
use Illuminate\Http\Request;

class PaymentMethodController extends Controller
{
    public function index($tenant_id,$company_id,$branch_id){

        return PaymentMethod::with('description')->where([
            'tenant_id' =>  $tenant_id,
            'company_id' => $company_id,
            'branch_id' =>  $branch_id
        ])->get();

    }

    public function store(Request $request){

        if(!$request->input('tenantId') || !$request->input('companyId') || !$request->input('branchId')) return 'error';

        $paymentMethod = PaymentMethod::create([
            'is_active' => $request->input('isActive') ,
            'tenant_id' => $request->input('tenantId'),
            'company_id' => $request->input('companyId'),
            'branch_id' => $request->input('branchId'),
            'created_by' => $request->input('createdBy')
        ]);

        if($paymentMethod){
            $i =0;
            foreach($request->input('description') as $description){
                $paymentMethodDescription[$i] = PaymentMethodDescription::create([
                    'payment_method_id' => $paymentMethod['id'],
                    'name' => $description['name'],
                    'desc' => isset($description['desc']) ? $description['desc'] : '' ,
                    'lang' => $description['lang']
                ]);
                $i++;
            }
        }
        $paymentMethod['description'] =  $paymentMethodDescription;
        return $paymentMethod;
    }


    public function update(Request $request,$id){

        //if(!$request->input('tenantId') || !$request->input('companyId') || !$request->input('branchId')) return 'error';

        $paymentMethod = PaymentMethod::find($id);

        if($paymentMethod){
            if($request->input('isActive') !== null){
                $paymentMethod->is_active = $request->input('isActive');
                $paymentMethod->save();
            }
            if($request->input('description')){

                $deleted = PaymentMethodDescription::where([
                    'payment_method_id' => $paymentMethod['id']
                ])->delete();

                //if($deleted){
                    $i =0;
                    foreach($request->input('description') as $description){
                        $paymentMethodDescription[$i] = PaymentMethodDescription::create([
                            'payment_method_id' => $paymentMethod['id'],
                            'name' => $description['name'],
                            'desc' => isset($description['desc']) ? $description['desc'] : '' ,
                            'lang' => $description['lang']
                        ]);
                        $i++;
                    }
                //}
                $paymentMethod['description'] =  $paymentMethodDescription;
            }
        }
        return $paymentMethod;
    }

    public function destroy($id){
        $payment_method = PaymentMethod::find($id);
        if(!$payment_method) return 'id not found';
        PaymentMethodDescription::where([
            'payment_method_id' => $payment_method['id']
        ])->delete();

        $payment_method->delete();
    }

}
