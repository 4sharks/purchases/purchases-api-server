<?php

namespace App\Http\Controllers\Inv;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\InvService;

class InvController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(InvService $invService,$tenant_id,$company_id,$branch_id)
    {
        return $invService->getAllInvoices($tenant_id,$company_id,$branch_id);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request , InvService $invService)
    {
        return $invService->createInv($request);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id,InvService $invService)
    {
        return $invService->showInv($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id , InvService $invService)
    {
        return $invService->updateInv($request,$id);
    }

    public function setPaid(Request $request, string $id , InvService $invService)
    {
        $result = $invService->setPaid($request,$id);
        if($result){
            return response()->json([
                "status" => 1 ,
                "result" => "success",
                "data" => $result
            ],201);
        }
        return response()->json([
            "status" => 0 ,
            "result" => "error",
            "data" => $result
        ],401);
    }

    /**
     * Remove the specified resource from storage.
     */

     public function destroy(string $id, InvService $invService)
    {
        return $invService->deleteInv($id);
    }

    public function get_last_invno_by_tenant(Request $request, InvService $invService){
        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');

        return $invService->getLastInvNoByTenant($tenant_id,$company_id,$branch_id);
    }

    public function get_last_invno_by_tenant_with_prefix(Request $request, InvService $invService){
        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');
        $prefix = $request->input('prefix');

        return $invService->getLastInvNoByTenant($tenant_id,$company_id,$branch_id,'with_prefix',$prefix);
    }

    public function sumInvoicesByMonth(InvService $invService,$tenant_id,$company_id,$branch_id,$month)
    {
        return $invService->getSumInvoicesByMonth($tenant_id,$company_id,$branch_id,$month);
    }

    public function sumInvoicesByYear(InvService $invService,$tenant_id,$company_id,$branch_id,$year)
    {
        return $invService->getSumInvoicesByYear($tenant_id,$company_id,$branch_id,$year);
    }

    public function sumInvoicesPaidByYear(InvService $invService,$tenant_id,$company_id,$branch_id,$year,$is_paid)
    {
        return $invService->getSumInvoicesPaidByYear($tenant_id,$company_id,$branch_id,$year,$is_paid);
    }

    public function sumGroupByMonth(InvService $invService,$tenant_id,$company_id,$branch_id,$year)
    {
        return $invService->getSumGroupByMonth($tenant_id,$company_id,$branch_id,$year);
    }

    public function sumBySalesman(InvService $invService,$tenant_id,$company_id,$branch_id,$year)
    {
        return $invService->getSumBySalesman($tenant_id,$company_id,$branch_id,$year);
    }

    public function topProducts(InvService $invService,$tenant_id,$company_id,$branch_id,$year)
    {
        return $invService->getTopProducts($tenant_id,$company_id,$branch_id,$year);
    }

    public function getByInvNo(InvService $invService,$tenant_id,$company_id,$branch_id,$inv_no)
    {
        return $invService->getByInvNo($tenant_id,$company_id,$branch_id,$inv_no);
    }

    public function getByCustomer(InvService $invService,$tenant_id,$company_id,$branch_id,$customer_id)
    {
        return $invService->getByCustomer($tenant_id,$company_id,$branch_id,$customer_id);
    }

    public function getFilterBy(InvService $invService,Request $request){
        $page_limit = $request->input('page_limit');

        $tenant_id = $request->input('tenant_id');
        $company_id = $request->input('company_id');
        $branch_id = $request->input('branch_id');
        $customer_id = $request->input('customer_id');
        $salesman_id = $request->input('salesman_id');
        $created_by = $request->input('created_by');
        $is_paid = $request->input('is_paid');
        $is_returned = $request->input('is_returned');
        $inv_tags = json_decode($request->input('inv_tags'));
        // $inv_tags = $request->input('inv_tags');
        $inv_start_date = null;
        $inv_end_date = null;

        if(!$tenant_id || !$company_id || !$branch_id){
            return response()->json([
                "status" => 0,
                "message" => 'Invalid fetch',
                "data" => [],
            ]);
        }

        if($request->input('inv_start_date')){
            $inv_start_date = date('Y-m-d',strtotime($request->input('inv_start_date')));
        }else{
            $inv_start_date = date('Y-m-d',strtotime(date('Y-01-01')));
        }

        if($request->input('inv_end_date')){
            $inv_end_date = date('Y-m-d',strtotime($request->input('inv_end_date')));
        }else{
            $inv_end_date = date('Y-m-d',strtotime(date('Y-12-31')));
        }

        $filter = [
            'page_limit' => $page_limit,
            'tenant_id' => $tenant_id,
            'company_id' => $company_id,
            'branch_id' => $branch_id,
            'customer_id' => $customer_id,
            'salesman_id' => $salesman_id,
            'inv_start_date' => $inv_start_date,
            'inv_end_date' => $inv_end_date,
            'is_paid' => $is_paid,
            'is_returned' => $is_returned,
            'created_by' => $created_by,
            'inv_tags' => $inv_tags,
        ];

        $fetch = $invService->filterBy($filter);

        if($fetch){
            return $fetch;
        }else{
            return response()->json([
                "status" => 0,
                "message" => 'Invalid fetch',
                "data" => [],
            ]);
        }
    }

}
