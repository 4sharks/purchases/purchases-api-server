<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('inv_id');
            $table->string('inv_no');
            $table->string('transaction_no')->nullable();
            $table->float('amount');
            $table->integer('payment_method_id');
            $table->string('notes')->nullable();
            $table->enum('type',['in','out'])->default('in')->nullable();
            $table->string('tenant_id')->nullable();
            $table->string('company_id')->nullable();
            $table->string('branch_id')->nullable();
            $table->string('created_by')->nullable(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payment_transactions');
    }
};
