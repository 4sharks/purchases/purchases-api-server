<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('quotes_details', function (Blueprint $table) {
            $table->id();
            $table->integer('inv_id');
            $table->string('inv_no');
            $table->string('product_id');
            $table->string('product_name');
            $table->integer('unit_id')->default(0);
            $table->tinyInteger('qty');
            $table->float('price');
            $table->float('total_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('quotes_details');
    }
};
